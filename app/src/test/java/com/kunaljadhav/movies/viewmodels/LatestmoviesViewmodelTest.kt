package com.kunaljadhav.movies.viewmodels

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.kunaljadhav.movies.model.Result
import com.kunaljadhav.movies.retrofit.APIInterface
import com.kunaljadhav.movies.utils.Constants
import com.kunaljadhav.movies.utils.NetworkStatus
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class LatestmoviesViewmodelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @Mock
    lateinit var apiInterface: APIInterface
    lateinit var latestmoviesViewmodel: LatestmoviesViewmodel
    private var testScheduler = TestScheduler() // Mock schedulers using RxJava TestScheduler.
    lateinit var resultObserver: Observer<Result>


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        latestmoviesViewmodel = LatestmoviesViewmodel()
    }

    @Test
    fun testGetLatestMovieApi() {
        val result = Result(1, Constants.Demo_MovieDesc, Constants.Demo_MovieTitle)
        Mockito.`when`(apiInterface.getLatestMovie(Constants.Themovie_Apikey))
            .thenReturn(Single.just(result))
        resultObserver = Mockito.mock(Observer::class.java) as Observer<Result>
        latestmoviesViewmodel.resultLiveData.observeForever(resultObserver)
        latestmoviesViewmodel.callGetLatestMovieApi(apiInterface, testScheduler)
        testScheduler.triggerActions()
        Mockito.verify(resultObserver).onChanged(result)
    }

    @Test
    fun testGetLatestMovieApiWithException(){
        Mockito.`when`(apiInterface.getLatestMovie(Constants.Themovie_Apikey))
            .thenReturn(Single.error(Exception()))
        latestmoviesViewmodel.callGetLatestMovieApi(apiInterface,testScheduler)
        testScheduler.triggerActions()
        Assert.assertEquals("java.lang.Exception",(latestmoviesViewmodel.networkStatusLiveData.value as NetworkStatus.Exception).exception.toString())
    }


}