package com.kunaljadhav.movies.viewmodels

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.kunaljadhav.movies.model.Movies
import com.kunaljadhav.movies.model.Result
import com.kunaljadhav.movies.retrofit.APIInterface
import com.kunaljadhav.movies.utils.Constants
import com.kunaljadhav.movies.utils.NetworkStatus
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MoviesByIDViewmodelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @Mock
    lateinit var apiInterface: APIInterface
    lateinit var moviesByIDViewmodel: MoviesByIDViewmodel
    private var testScheduler = TestScheduler() // Mock schedulers using RxJava TestScheduler.
    lateinit var networkStatusObserver: Observer<NetworkStatus>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        moviesByIDViewmodel = MoviesByIDViewmodel()
    }

    @Test
    fun testSuccess() {
        var searchString = "3"
        var result = Result(1, Constants.Demo_MovieTitle, Constants.Demo_MovieDesc)
        var moviesResponse = Movies(1, listOf(result), null, null, null, null)
        Mockito.`when`(apiInterface.getSimilarMovies(searchString, Constants.Themovie_Apikey))
            .thenReturn(Single.just(moviesResponse))
        moviesByIDViewmodel.resgisterSearchStringSubscription(apiInterface, Observable.just(searchString), testScheduler)
            .observeOn(testScheduler)
            .subscribe {  Assert.assertEquals(listOf(result), it)}
      }
    @Test
    fun testWithResultNull(){
        networkStatusObserver = Mockito.mock(Observer::class.java) as Observer<NetworkStatus>
        var searchString = "a"
        var moviesResponse = Movies(1, null, null, null, null, null)
        Mockito.`when`(apiInterface.getSimilarMovies(searchString, Constants.Themovie_Apikey))
            .thenReturn(Single.just(moviesResponse))
        networkStatusObserver = Mockito.mock(Observer::class.java) as Observer<NetworkStatus>
        moviesByIDViewmodel.networkStatusLiveData.observeForever(networkStatusObserver)
        moviesByIDViewmodel.resgisterSearchStringSubscription(apiInterface, Observable.just(searchString), testScheduler)
            .observeOn(testScheduler)
            .subscribe { }
        testScheduler.triggerActions()
        Mockito.verify(networkStatusObserver).onChanged(NetworkStatus.Error(Constants.error_noData))

    }

    @Test
    fun testWithStatusError(){
        networkStatusObserver = Mockito.mock(Observer::class.java) as Observer<NetworkStatus>
        var searchString = "a"
        var result = Result(1, "", "")
        var moviesResponse = Movies(1, listOf(result), null, null, Constants.NOResourceFound_Errorcode, Constants.NOResourceFound_ErrorMessage)
        Mockito.`when`(apiInterface.getSimilarMovies(searchString, Constants.Themovie_Apikey))
            .thenReturn(Single.just(moviesResponse))
        networkStatusObserver = Mockito.mock(Observer::class.java) as Observer<NetworkStatus>
        moviesByIDViewmodel.networkStatusLiveData.observeForever(networkStatusObserver)
        moviesByIDViewmodel.resgisterSearchStringSubscription(apiInterface, Observable.just(searchString), testScheduler)
            .observeOn(testScheduler)
            .subscribe { }
        testScheduler.triggerActions()
        Mockito.verify(networkStatusObserver).onChanged(NetworkStatus.Error(Constants.NOResourceFound_ErrorMessage))

    }

    @Test
    fun testWithException(){
        var searchString = "/*^%$"
        Mockito.`when`(apiInterface.getSimilarMovies(searchString, Constants.Themovie_Apikey))
            .thenReturn(Single.error(Exception()))
        networkStatusObserver = Mockito.mock(Observer::class.java) as Observer<NetworkStatus>
        moviesByIDViewmodel.networkStatusLiveData.observeForever(networkStatusObserver)
        moviesByIDViewmodel.resgisterSearchStringSubscription(apiInterface, Observable.just(searchString), testScheduler)
            .observeOn(testScheduler)
            .subscribe { }
        testScheduler.triggerActions()
        Assert.assertEquals("java.lang.Exception",(moviesByIDViewmodel.networkStatusLiveData.value as NetworkStatus.Exception).exception.toString())
    }
}