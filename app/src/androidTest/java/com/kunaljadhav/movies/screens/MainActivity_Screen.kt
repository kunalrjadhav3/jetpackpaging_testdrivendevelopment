package com.kunaljadhav.movies.screens

import android.view.View
import com.agoda.kakao.*
import com.kunaljadhav.movies.R
import org.hamcrest.Matcher

class MainActivity_Screen : Screen<MainActivity_Screen>() {

    val rv_movies: KRecyclerView = KRecyclerView({
        withId(R.id.rv_movies)
    }, itemTypeBuilder = {
        itemType(MainActivity_Screen::ItemMovies)
    })


    val rv_favmovies: KRecyclerView = KRecyclerView({
        withId(R.id.rv_favmovies)
    }, itemTypeBuilder = {
        itemType(MainActivity_Screen::ItemMovies)
    })

    class ItemMovies(parent: Matcher<View>) : KRecyclerItem<ItemMovies>(parent) {
        val ivAddFav: KImageView = KImageView(parent) { withId(R.id.ivAddFav) }

    }

    val tablayout: KTabLayout = KTabLayout({
        withId(R.id.tablayout)
    })



    val fab: KButton = KButton({
        withId(R.id.fab)
    })

}