package com.kunaljadhav.movies.topLevelFunctions

import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.matcher.ViewMatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

fun typeTextWithDelay(text:String, hint:String= "Search movies by id") {
    Espresso.onView(ViewMatchers.withHint(hint)).perform(
        ViewActions.typeText(
            runBlocking {
                delay(500)
                return@runBlocking text
            })
    )
}

