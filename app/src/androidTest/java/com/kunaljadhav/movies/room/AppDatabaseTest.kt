package com.kunaljadhav.movies.room

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.kunaljadhav.movies.utils.Constants
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AppDatabaseTest {

    private val dbInstance: AppDatabase by lazy { AppDatabase.getInstance(InstrumentationRegistry.getTargetContext())!! }
    val favMoviesPojo = FavMoviesPojo(1,Constants.Demo_MovieTitle, Constants.Demo_MovieDesc)

    @Test
    fun test_insert_FavMovie() {

        dbInstance.resultDao().insert(favMoviesPojo)
        val objectFromDB = dbInstance.resultDao().getByMovieID(favMoviesPojo.id)
        assertEquals(favMoviesPojo, objectFromDB)
    }

    @Test
    fun insertAndFetchTest() {
        val objectFromDB = dbInstance.resultDao().insertAndFetch(favMoviesPojo)
        assertEquals(favMoviesPojo, objectFromDB)
        assertEquals(null, dbInstance.resultDao().insertAndFetch(favMoviesPojo))
    }

    @Test
    fun deleteAndCheckTest() {
        dbInstance.resultDao().insert(favMoviesPojo)
        assertEquals(true, dbInstance.resultDao().deleteAndCheck(favMoviesPojo))
        assertEquals(false, dbInstance.resultDao().deleteAndCheck(favMoviesPojo))
    }

    @After
    fun tearDown() {
        dbInstance.clearAllTables()
        dbInstance.destroyInstance()

    }
}