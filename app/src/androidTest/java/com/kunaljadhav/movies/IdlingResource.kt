package com.kunaljadhav.movies

import android.support.test.espresso.IdlingResource
import android.support.test.espresso.idling.CountingIdlingResource

/*If you are using thread and any other background process then you also might want to test your code
 with the same behavior and tell espresso to wait until the tasks are finished.*/
class IdlingResource {
    companion object {
        private val RESOURCE = "GLOBAL"

        private val mCountingIdlingResource = CountingIdlingResource(RESOURCE)

        fun increment() {
            mCountingIdlingResource.increment()
        }

        fun decrement() {
            mCountingIdlingResource.decrement()
        }

        fun getIdlingResource(): IdlingResource {
            return mCountingIdlingResource
        }
    }
}