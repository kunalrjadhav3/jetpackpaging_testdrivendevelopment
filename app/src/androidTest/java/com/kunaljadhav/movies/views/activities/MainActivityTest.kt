package com.kunaljadhav.movies.views.activities

import android.support.test.espresso.Espresso
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.agoda.kakao.KRecyclerView
import com.kunaljadhav.movies.IdlingResource
import com.kunaljadhav.movies.R
import com.kunaljadhav.movies.screens.MainActivity_Screen
import com.kunaljadhav.movies.topLevelFunctions.typeTextWithDelay
import com.kunaljadhav.movies.utils.Constants
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java)
    val screen: MainActivity_Screen by lazy { MainActivity_Screen() }

    @Before
    fun setUp() {
        IdlingRegistry.getInstance()
            .register(IdlingResource.getIdlingResource()); // make Espresso aware of asynchronous tasks, to wait before calling ViewMatchers
    }

    @Test
    fun test() {
        startMainActivityTest()
        startSearchMoviesByIDTest()
    }

    private fun startMainActivityTest() {
        screen {
            rv_movies {
                isVisible()
                hasSize(40) // Check if 2 pages are loaded
                for (i in 5..9)
                    clickrv_moviesRowItem(this, i) // add movies to DB (To Favourites)
             }

            runBlocking { delay(500) } //  pause UI thread for 500 ms
            tablayout {
                selectTab(1) // Switch from All tab to Favourites tab
            }

            rv_favmovies {
                isVisible()
                for (i in 0..4) {
                    runBlocking { delay(1000) }
                    clickrv_favmoviesRowItem(this) // remove movies from DB (To Favourites)
                }

            }
            runBlocking { delay(500) }
            tablayout {// swap from All tab to Favourites tab (To Favourites)
                runBlocking { delay(1000) }
                selectTab(0)
                runBlocking { delay(1000) }
                selectTab(1)
                runBlocking { delay(1000) }
            }
            fab {
                isVisible()
                click()
            }
        }

    }

    private fun clickrv_moviesRowItem(rv: KRecyclerView, position: Int) {
        rv {
            scrollTo(position)
            childAt<MainActivity_Screen.ItemMovies>(position, {
                isVisible()
                ivAddFav {
                    isDisplayed()
                    click()
                }
            })
        }

    }

    private fun clickrv_favmoviesRowItem(rv: KRecyclerView) {
        rv {
            scrollTo(0)
            childAt<MainActivity_Screen.ItemMovies>(0, {
                isVisible()
                ivAddFav {
                    isDisplayed()
                    click()
                }
            })
        }

    }

    private fun startSearchMoviesByIDTest() {
        Espresso.onView(ViewMatchers.withId(R.id.action_search)).perform(ViewActions.click())
        for (i in 1..4)
            typeTextWithDelay(i.toString())

        runBlocking { delay(1000) }
        Espresso.onView(ViewMatchers.withHint(Constants.SearchView_Edittext_Hint)).perform(ViewActions.clearText())

        val numberList = arrayListOf(3, 2, 0, 8)
        for (i in 0..numberList.size - 1)
            typeTextWithDelay(numberList[i].toString())

        runBlocking { delay(1000) }

    }


    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(IdlingResource.getIdlingResource())
    }
}