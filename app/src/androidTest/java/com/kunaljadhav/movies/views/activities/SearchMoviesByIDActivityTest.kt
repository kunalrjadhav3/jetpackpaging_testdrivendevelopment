package com.kunaljadhav.movies.views.activities

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.matcher.ViewMatchers.withHint
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.kunaljadhav.movies.IdlingResource
import com.kunaljadhav.movies.R
import com.kunaljadhav.movies.topLevelFunctions.typeTextWithDelay
import com.kunaljadhav.movies.utils.Constants
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SearchMoviesByIDActivityTest {
    @Rule
    @JvmField
    val rule = ActivityTestRule(SearchMoviesByIDActivity::class.java)

    @Before
    fun setUp() {
        IdlingRegistry.getInstance()
            .register(IdlingResource.getIdlingResource()); // make Espresso aware of asynchronous tasks, to wait before calling ViewMatchers

    }

    @Test
    fun test() {

        onView(withId(R.id.action_search)).perform(ViewActions.click()) // click search icon
        for (i in 1..4){
            typeTextWithDelay(i.toString())
        }

        runBlocking { delay(1000) }
        onView(withHint(Constants.SearchView_Edittext_Hint)).perform(ViewActions.clearText()) // clear text of searchbar

        val numberList = arrayListOf(3, 2, 0, 8)
        for (i in 0..numberList.size - 1){
            typeTextWithDelay(numberList[i].toString())
        }

        runBlocking { delay(1000) }
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(IdlingResource.getIdlingResource())
    }
}