package com.kunaljadhav.movies

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.kunaljadhav.movies.di.ApplicationComponent
import com.kunaljadhav.movies.di.ApplicationModule
import com.kunaljadhav.movies.di.DaggerApplicationComponent
import io.fabric.sdk.android.Fabric

class MainApp: Application() {

    companion object {
        lateinit var applicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        applicationComponent = initDagger()
        Fabric.with(this, Crashlytics()) //init for Frabic beta release use in CI & Crashlytics
    }

    /**
     * initialize dependencies with Singleton scope
     */
    private fun initDagger(): ApplicationComponent {
        var applicationComponent = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this))
            .build()
        return applicationComponent
    }

}