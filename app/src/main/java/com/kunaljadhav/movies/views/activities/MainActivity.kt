package com.kunaljadhav.movies.views.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.kunaljadhav.movies.R
import com.kunaljadhav.movies.utils.Constants
import com.kunaljadhav.movies.views.adapters.TabsAdapter
import com.kunaljadhav.movies.views.fragments.FavMoviesFragment
import com.kunaljadhav.movies.views.fragments.MoviesFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawer)
        setSupportActionBar(toolBar)
        initDrawer(toolBar)
        initTabAdapter()
        fab.setOnClickListener { startActivity(Intent(this, SearchMoviesByIDActivity::class.java)) }
    }

    private fun initDrawer(toolBar: Toolbar) {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolBar,R.string.drawer_open, R.string.drawer_closed
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
    }

    private fun initTabAdapter() {
        val tabsAdapter = TabsAdapter(supportFragmentManager)
        tabsAdapter.addFragment(MoviesFragment(), Constants.All_Movies_Title)
        tabsAdapter.addFragment(FavMoviesFragment(), Constants.Fav_Movies_Title)
        viewPager.adapter = tabsAdapter
        tablayout.setupWithViewPager(viewPager)


    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                // Handle the camera action
            }
            R.id.nav_search -> {
                startActivity(Intent(this, SearchMoviesByIDActivity::class.java))
            }


        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

}
