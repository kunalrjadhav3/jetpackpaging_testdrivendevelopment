package com.kunaljadhav.movies.views.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class TabsAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    val fragmentsList by lazy { mutableListOf<Fragment>() }
    val fragmentsTitleList by lazy { mutableListOf<String>() }

    override fun getItem(position: Int) = fragmentsList.get(position)

    override fun getCount() = fragmentsList.size

    fun addFragment(fragment: Fragment, title: String) {
        fragmentsList.add(fragment)
        fragmentsTitleList.add(title)

    }

    override fun getPageTitle(position: Int) = fragmentsTitleList.get(position)
}