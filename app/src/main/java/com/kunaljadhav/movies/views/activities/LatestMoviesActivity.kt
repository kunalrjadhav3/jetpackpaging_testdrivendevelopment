package com.kunaljadhav.movies.views.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import com.kunaljadhav.movies.R
import com.kunaljadhav.movies.utils.NetworkStatus
import com.kunaljadhav.movies.utils.Utility
import com.kunaljadhav.movies.viewmodels.LatestmoviesViewmodel
import kotlinx.android.synthetic.main.activity_new_movies.*
import kotlinx.android.synthetic.main.item_movies.*
import kotlinx.android.synthetic.main.layout_progressbar.*
import kotlinx.android.synthetic.main.toolbar_searchview.*


class LatestMoviesActivity : BaseActivity() {
    val latestmoviesViewmodel: LatestmoviesViewmodel by lazy {
        ViewModelProviders.of(this).get(LatestmoviesViewmodel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_movies)
        setSupportActionBar(toolBar)
        setViewObservers()
        ivAddFav.visibility=View.GONE
        latestmoviesViewmodel.callGetLatestMovieApi(apiInterface)
    }

    private fun setViewObservers() {
        latestmoviesViewmodel.resultLiveData.observeForever(Observer {
            txt_movie_name.text=it!!.title
            txt_movie_overview.text=it!!.overview
        })
        latestmoviesViewmodel.networkStatusLiveData.observe(this, android.arch.lifecycle.Observer {
            when (it) {
                is NetworkStatus.Loading -> {
                    setProgressbarVisibility(true)
                }

                is NetworkStatus.Loaded -> {
                    setProgressbarVisibility(false)
                }
                is NetworkStatus.Error -> {
                    displayMessage(it.message)
                }
                is NetworkStatus.Exception -> {
                    displayMessage(it.exception.localizedMessage)
                }
            }
        })
    }

    private fun setProgressbarVisibility(show: Boolean) {
        if (show) progress.visibility = View.VISIBLE else progress.visibility = View.GONE

    }

    private fun displayMessage(message: String) {
        Utility.displaySnackbar(latestmovies_container, message)

    }

}
