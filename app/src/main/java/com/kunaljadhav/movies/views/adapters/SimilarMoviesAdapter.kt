package com.kunaljadhav.movies.views.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.kunaljadhav.movies.R
import com.kunaljadhav.movies.model.Result

class SimilarMoviesAdapter(var similarMoviesList:ArrayList<Result>): RecyclerView.Adapter<SimilarMoviesAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_similarmovies, parent, false);
        return MyViewHolder(itemView);
    }

    override fun getItemCount()=similarMoviesList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.movieTitle.text = similarMoviesList.get(position).title
        holder.movieOverview.text = similarMoviesList.get(position).overview
        }


    inner class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var movieTitle: TextView = itemView!!.findViewById(R.id.txt_movie_name)
        var movieOverview: TextView = itemView!!.findViewById(R.id.txt_movie_overview)
    }
}