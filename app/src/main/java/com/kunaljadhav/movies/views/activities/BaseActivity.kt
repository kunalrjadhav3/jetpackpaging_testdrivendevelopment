package com.kunaljadhav.movies.views.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar
import com.kunaljadhav.movies.MainApp
import com.kunaljadhav.movies.retrofit.APIInterface
import javax.inject.Inject

open class BaseActivity: AppCompatActivity() {
    @Inject
    lateinit var apiInterface: APIInterface
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApp.applicationComponent.inject(this)
    }

     fun setProgressbarVisibility(progressBar:ProgressBar, show: Boolean) {
        if (show) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE

    }
}