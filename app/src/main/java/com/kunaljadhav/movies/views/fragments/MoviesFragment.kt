package com.kunaljadhav.movies.views.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.kunaljadhav.movies.R
import com.kunaljadhav.movies.room.FavMoviesPojo
import com.kunaljadhav.movies.utils.NetworkStatus
import com.kunaljadhav.movies.utils.Utility
import com.kunaljadhav.movies.viewmodels.MoviesListViewModel
import com.kunaljadhav.movies.views.adapters.MoviesAdapter
import kotlinx.android.synthetic.main.fragment_movies.*

class MoviesFragment : BaseFragment(){

    val  moviesListViewModel by lazy { ViewModelProviders.of(activity!!).get(MoviesListViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        moviesListViewModel.initMoviesDataSourceFactory(apiInterface)
        initAdapter()
        setViewObservers()
    }

    private fun initAdapter() {
        val moviesListAdapter = MoviesAdapter(object :
            MoviesAdapter.addBtnClickListener {
            override fun onAddBtnClick(favMoviesPojo: FavMoviesPojo) {
                moviesListViewModel.addMovieToDb(appDatabase,favMoviesPojo)
            }
        })
        rv_movies.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
        rv_movies.adapter = moviesListAdapter
        moviesListViewModel.moviesList.observe(this, Observer {
            moviesListAdapter.submitList(it) //update recyclerview with updated Page from API
        })
    }

    private fun setViewObservers() {
        moviesListViewModel.networkStatusLiveData.observe(this, Observer {
            when (it) {
                is NetworkStatus.Loading ->
                    setProgressbarVisibility(progressBar,true)

                is NetworkStatus.Loaded -> {
                    displayMessage(it.pageNo)
                    setProgressbarVisibility(progressBar,false)
                }
                is NetworkStatus.Error -> {
                    setProgressbarVisibility(progressBar,false)
                    displayMessage(it.message)
                }
                is NetworkStatus.Exception -> {
                    setProgressbarVisibility(progressBar,false)
                    displayMessage(it.exception.localizedMessage)

                }
            }
        })
    }


    fun displayMessage(errorMessage: String) {
        Utility.displaySnackbar(mainContainer,errorMessage)
    }
}