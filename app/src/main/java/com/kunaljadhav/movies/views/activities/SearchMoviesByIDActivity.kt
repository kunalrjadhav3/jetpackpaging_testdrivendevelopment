package com.kunaljadhav.movies.views.activities


import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.View
import com.kunaljadhav.movies.R
import com.kunaljadhav.movies.model.Result
import com.kunaljadhav.movies.retrofit.APIInterface
import com.kunaljadhav.movies.utils.NetworkStatus
import com.kunaljadhav.movies.viewmodels.MoviesByIDViewmodel
import com.kunaljadhav.movies.views.adapters.SimilarMoviesAdapter
import com.miguelcatalan.materialsearchview.MaterialSearchView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_searchby_id.*
import kotlinx.android.synthetic.main.layout_progressbar.*
import kotlinx.android.synthetic.main.toolbar_searchview.*

class SearchMoviesByIDActivity : BaseActivity() {

    val moviesByIDViewmodel by lazy { ViewModelProviders.of(this).get(MoviesByIDViewmodel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_searchby_id)
        setSupportActionBar(toolBar)
        setViewObservers()
        registerStringObservabletoObserver(SearhViewObservable(),apiInterface)

    }

    private fun setViewObservers() {
        moviesByIDViewmodel.networkStatusLiveData.observe(this, android.arch.lifecycle.Observer {
            when (it) {
                is NetworkStatus.Loading -> {
                    setRecyclerVisibility(false)
                    setTvMessageVisibility(false)
                    setProgressbarVisibility(progress,true)
                }

                is NetworkStatus.Loaded -> {
                    setProgressbarVisibility(progress,false)
                    setTvMessageVisibility(false)
                    setRecyclerVisibility(true)
                }
                is NetworkStatus.Error -> {
                    setProgressbarVisibility(progress,false)
                    setRecyclerVisibility(false)
                    setTvMessageVisibility(true)
                    tv_message.text = it.message
                }
                is NetworkStatus.Exception -> {
                    setProgressbarVisibility(progress,false)
                    setRecyclerVisibility(false)
                    setTvMessageVisibility(true)
                    tv_message.text = it.exception.localizedMessage
                }
            }
        })
    }

    private fun setRecyclerVisibility(show: Boolean) {
        if (show) rv_similar_movies.visibility = View.VISIBLE else rv_similar_movies.visibility = View.GONE

    }



    fun setTvMessageVisibility(show: Boolean) {
        if (show) tv_message.visibility = View.VISIBLE else tv_message.visibility = View.GONE
    }


    internal fun registerStringObservabletoObserver(stringObservable: Observable<String>, apiInterface: APIInterface) {
        moviesByIDViewmodel.resgisterSearchStringSubscription(apiInterface,stringObservable).observeOn(AndroidSchedulers.mainThread()).subscribe(getObserver())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val item = menu.findItem(R.id.action_search)
        search_view.setMenuItem(item)
        return true
    }



    private fun getObserver(): io.reactivex.Observer<List<Result>> {
        return object : io.reactivex.Observer<List<Result>> {
            override fun onComplete() {}
            override fun onError(e: Throwable) {}
            override fun onNext(searchResultList: List<Result>) {
                if (rv_similar_movies.adapter == null) {
                    initRecyclerAdapter(searchResultList)
                } else if (rv_similar_movies.adapter is SimilarMoviesAdapter) {
                    val adapterInstance = rv_similar_movies.adapter as SimilarMoviesAdapter
                    adapterInstance.similarMoviesList.clear()
                    adapterInstance.similarMoviesList.addAll(searchResultList)
                    adapterInstance.notifyDataSetChanged()
                }

            }

            override fun onSubscribe(d: Disposable) {}
        }

    }

    private fun initRecyclerAdapter(searchResultList: List<Result>) {
        var adapter = SimilarMoviesAdapter(searchResultList as ArrayList<Result>)
        var mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        rv_similar_movies.setLayoutManager(mLayoutManager)
        rv_similar_movies.setItemAnimator(DefaultItemAnimator())
        rv_similar_movies.setAdapter(adapter)

    }


    private fun SearhViewObservable(): Observable<String> {
        return Observable.create { e ->
            search_view.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(text: String): Boolean {
                    e.onNext(text)
                    return false
                }

                override fun onQueryTextChange(text: String): Boolean {
                    if(text.equals("")){
                        setRecyclerVisibility(false)
                        setTvMessageVisibility(false)
                    } else
                    e.onNext(text)
                    return false
                }
            })
        }

    }

}
