package com.kunaljadhav.movies.views.adapters

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.kunaljadhav.movies.R
import com.kunaljadhav.movies.model.Result
import com.kunaljadhav.movies.room.FavMoviesPojo

class MoviesAdapter(val listener: addBtnClickListener) :
    PagedListAdapter<Result, RecyclerView.ViewHolder>(ResultsDiffCallback) {


    interface addBtnClickListener {
        fun onAddBtnClick(favMoviesPojo: FavMoviesPojo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_movies, parent, false)
        return MyViewHolder(view)
    }

    inner class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var movieTitle: TextView = itemView!!.findViewById(R.id.txt_movie_name)
        var movieOverview: TextView = itemView!!.findViewById(R.id.txt_movie_overview)
        var ivAddRemoveFav: ImageView = itemView!!.findViewById(R.id.ivAddFav)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyViewHolder).movieTitle.text = getItem(position)!!.title
        holder.movieOverview.text = getItem(position)!!.overview
        setIvAddRemoveFavDrawable(holder)
        setOnClickListener(holder,getItem(position)!!.id)
    }

    private fun setIvAddRemoveFavDrawable(holder: MyViewHolder) {
        holder.ivAddRemoveFav.setImageResource(R.drawable.ic_plus)


    }

    private fun setOnClickListener(
        holder: MyViewHolder,
        id: Int?
    ) {
        holder.ivAddRemoveFav.setOnClickListener { listener.onAddBtnClick( // User checked, add to Favorites
            FavMoviesPojo(
                id!!,holder.movieTitle.text.toString(),
                holder.movieOverview.text.toString()
            )
        ) }

    }

    companion object {
        val ResultsDiffCallback = object : DiffUtil.ItemCallback<Result>() {
            override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
                return oldItem.title == newItem.title
            }
            override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
                return oldItem == newItem
            }
        }
    }
}