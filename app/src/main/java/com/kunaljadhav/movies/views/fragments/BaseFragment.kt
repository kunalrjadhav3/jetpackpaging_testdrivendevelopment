package com.kunaljadhav.movies.views.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.widget.ProgressBar
import com.kunaljadhav.movies.MainApp
import com.kunaljadhav.movies.retrofit.APIInterface
import com.kunaljadhav.movies.room.AppDatabase
import javax.inject.Inject

open class BaseFragment: Fragment() {
    @Inject
    lateinit var apiInterface: APIInterface
    @Inject
    lateinit var appDatabase: AppDatabase
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApp.applicationComponent.inject(this) // provide injects to Children of BaseFragment

    }

     fun setProgressbarVisibility(progressBar: ProgressBar,show: Boolean) {
        if (show) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE

    }
}