package com.kunaljadhav.movies.views.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.kunaljadhav.movies.R
import com.kunaljadhav.movies.room.FavMoviesPojo

class FavMoviesAdapter(var favMovieList:ArrayList<FavMoviesPojo>,val listener:removeBtnClickListener): RecyclerView.Adapter<FavMoviesAdapter.MyViewHolder>() {



    interface removeBtnClickListener {
        fun onRemoveBtnClick(favMoviesPojo: FavMoviesPojo)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.movieTitle.text = favMovieList.get(position).movieTitle
        holder.movieOverview.text = favMovieList.get(position).movieDesc
        setIvAddRemoveFavDrawable(holder)
        setOnClickListener(holder,favMovieList.get(position).id)
    }

    override fun getItemCount()=favMovieList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_movies, parent, false);
        return MyViewHolder(itemView);
    }

    inner class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var movieTitle: TextView = itemView!!.findViewById(R.id.txt_movie_name)
        var movieOverview: TextView = itemView!!.findViewById(R.id.txt_movie_overview)
        var ivAddRemoveFav: ImageView = itemView!!.findViewById(R.id.ivAddFav)
    }

    private fun setOnClickListener(
        holder: MyViewHolder,
        id: Int
    ) {
        holder.ivAddRemoveFav.setOnClickListener { listener.onRemoveBtnClick( // User checked, add to Favorites
            FavMoviesPojo(
                id,holder.movieTitle.text.toString(),
                holder.movieOverview.text.toString()
            )
        ) }

    }

    private fun setIvAddRemoveFavDrawable(holder: MyViewHolder) {
        holder.ivAddRemoveFav.setImageResource(R.drawable.ic_action_navigation_close)


    }
}