package com.kunaljadhav.movies.views.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.kunaljadhav.movies.R
import com.kunaljadhav.movies.repository.DBRepository
import com.kunaljadhav.movies.room.FavMoviesPojo
import com.kunaljadhav.movies.utils.DBRecordStatus
import com.kunaljadhav.movies.viewmodels.MoviesListViewModel
import com.kunaljadhav.movies.views.adapters.FavMoviesAdapter
import kotlinx.android.synthetic.main.fragment_favmovies.*

class FavMoviesFragment : BaseFragment() {

    val moviesListViewModel by lazy { ViewModelProviders.of(activity!!).get(MoviesListViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favmovies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        DBRepository.getAllFavMoviesFromDB(appDatabase).let {
            var favMoviesAdapter =
                FavMoviesAdapter(it as ArrayList<FavMoviesPojo>, object : FavMoviesAdapter.removeBtnClickListener {
                    override fun onRemoveBtnClick(favMoviesPojo: FavMoviesPojo) {
                        moviesListViewModel.deleteMovieFromDb(appDatabase, favMoviesPojo)

                    }
                })
            rv_favmovies.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            rv_favmovies.adapter = favMoviesAdapter
        }
        setViewObservers()
    }

    private fun setViewObservers() {
        moviesListViewModel.dBRecordStatusLiveData.observe(this, Observer {
            when (it) {
                is DBRecordStatus.RecordInserted -> { // add record to rv_favmovies
                    if (rv_favmovies.adapter is FavMoviesAdapter) {
                        (rv_favmovies.adapter as FavMoviesAdapter).favMovieList.add(it.favMoviesPojo)
                        rv_favmovies.adapter.notifyItemInserted((rv_favmovies.adapter as FavMoviesAdapter).favMovieList.size - 1)
                    }
                }
                is DBRecordStatus.RecordDeleted -> { // remove record from rv_favmovies
                    if (rv_favmovies.adapter is FavMoviesAdapter) {
                        val position = (rv_favmovies.adapter as FavMoviesAdapter).favMovieList.indexOf(it.favMoviesPojo)
                        (rv_favmovies.adapter as FavMoviesAdapter).favMovieList.removeAt(position)
                        rv_favmovies.adapter.notifyItemRemoved(position)

                    }
                }
            }


        })
    }
}