package com.kunaljadhav.movies.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import android.database.sqlite.SQLiteConstraintException

@Dao
interface Dao {

    @Query("SELECT * FROM FavMoviesPojo")
    fun getAll(): List<FavMoviesPojo>

    @Insert
    fun insert(results: FavMoviesPojo): Long

    @Query("DELETE FROM FavMoviesPojo WHERE id = :id")
    fun deleteByMovieID(id: Int): Int

    @Query("SELECT * FROM FavMoviesPojo WHERE id = :id LIMIT 1")
    fun getByMovieID(id: Int): FavMoviesPojo?

    @Transaction
    fun insertAndFetch(favMovie: FavMoviesPojo): FavMoviesPojo? {
        try {
            insert(favMovie) // Try inserting record. If inserted, return read record from DB, else record already exits so return null.
            return getByMovieID(favMovie.id)
        } catch (e: SQLiteConstraintException) {
            return null
        }
    }

    @Transaction
    fun deleteAndCheck(favMovie: FavMoviesPojo): Boolean { // Try deleting record. If deleted, return True, else False
        if (deleteByMovieID(favMovie.id) > 0)
            return true
        return false

    }
}