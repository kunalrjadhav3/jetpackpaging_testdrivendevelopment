package com.kunaljadhav.movies.room

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "FavMoviesPojo")
data class FavMoviesPojo(@PrimaryKey val id:Int, val movieTitle:String, val movieDesc:String)