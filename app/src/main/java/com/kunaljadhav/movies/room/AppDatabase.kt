package com.kunaljadhav.movies.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.kunaljadhav.movies.utils.Constants

@Database(entities = arrayOf(FavMoviesPojo::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun resultDao(): Dao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        AppDatabase::class.java, Constants.DBName)
                        .build()
                }
            }
            return INSTANCE
        }

    }
    fun destroyInstance() {
        INSTANCE = null
    }
}