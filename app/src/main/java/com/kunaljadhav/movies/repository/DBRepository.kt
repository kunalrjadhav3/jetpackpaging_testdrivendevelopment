package com.kunaljadhav.movies.repository

import com.kunaljadhav.movies.room.AppDatabase
import com.kunaljadhav.movies.room.FavMoviesPojo
import kotlinx.coroutines.*

/*
* runBlocking coroutine -> use it, if you want program execution to be synchronous
* GlobalScope coroutine -> use it, if you want program execution to be asynchronous
* */
object DBRepository {

    fun startInsertandFetchTransaction(db: AppDatabase, favMovie: FavMoviesPojo): FavMoviesPojo? {
        return runBlocking {
            withContext(Dispatchers.IO) { db.resultDao().insertAndFetch(favMovie) }
        }
    }

    fun startDeleteandCheckTransaction(db: AppDatabase, favMovie: FavMoviesPojo): Boolean {
        return runBlocking {
            withContext(Dispatchers.IO) { db.resultDao().deleteAndCheck(favMovie) }
        }
    }

    fun getAllFavMoviesFromDB(db: AppDatabase): List<FavMoviesPojo> {
        return runBlocking {
            withContext(Dispatchers.IO) { db.resultDao().getAll() }
        }  // runBlocking is used, because we want getAllResultsFromDB() operation
        // to be finished first, before moving to next line of getAllResultsFromDB() caller code }
    }

    fun deleteMovie(db: AppDatabase, id: Int) {
        GlobalScope.launch { db.resultDao().deleteByMovieID(id) }
    }

    fun getFavMovie(db: AppDatabase, id: Int): FavMoviesPojo? {
        return runBlocking { GlobalScope.async { db.resultDao().getByMovieID(id) }.await() }
    }
}



