package com.kunaljadhav.movies.model


import com.google.gson.annotations.SerializedName

data class Movies(
    @SerializedName("page")
    var page: Int?,
    @SerializedName("results")
    var results: List<Result?>?,
    @SerializedName("total_pages")
    var totalPages: Int?,
    @SerializedName("total_results")
    var totalResults: Int?,
    var status_code:Int?,
    var status_message:String?
)