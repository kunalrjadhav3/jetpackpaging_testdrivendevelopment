package com.kunaljadhav.movies.model


import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("id")
    var id: Int?,
    @SerializedName("overview")
    var overview: String?,
    @SerializedName("title")
    var title: String?
)