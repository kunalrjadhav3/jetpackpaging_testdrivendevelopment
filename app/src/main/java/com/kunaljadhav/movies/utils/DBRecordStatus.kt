package com.kunaljadhav.movies.utils

import com.kunaljadhav.movies.room.FavMoviesPojo

sealed class DBRecordStatus {
    data class RecordInserted(val favMoviesPojo: FavMoviesPojo):DBRecordStatus()
    data class RecordDeleted(val favMoviesPojo: FavMoviesPojo):DBRecordStatus()
}