package com.kunaljadhav.movies.utils

sealed class NetworkStatus {
    class Loading() : NetworkStatus()
    class Loaded(val pageNo:String) : NetworkStatus()
    data class Error(val message: String) : NetworkStatus()
    class Exception(val exception: Throwable) : NetworkStatus()
}