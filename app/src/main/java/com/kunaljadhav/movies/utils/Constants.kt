package com.kunaljadhav.movies.utils

class Constants {
    companion object{

        const val DBName="MoviesDB"
        const val BASE_URL = "https://api.themoviedb.org";
        const val Themovie_Apikey="8fa3ef535ba173991ed6aa3ffc6a0943"
        const val error_noData="No data"
        const val pageLoaded="Page Loaded : "
        const val All_Movies_Title="All"
        const val Fav_Movies_Title="Favourites"
        const val NOResourceFound_Errorcode=34
        const val NOResourceFound_ErrorMessage="No resource found"
        const val SearchView_Edittext_Hint = "Search movies by id"
        const val Demo_MovieTitle="Sully"
        const val Demo_MovieDesc="Tom Hanks"

    }
}