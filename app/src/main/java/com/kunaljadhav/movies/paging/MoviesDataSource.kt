package com.kunaljadhav.movies.paging

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import com.kunaljadhav.movies.model.Result
import com.kunaljadhav.movies.retrofit.APIInterface
import com.kunaljadhav.movies.utils.Constants
import com.kunaljadhav.movies.utils.NetworkStatus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MoviesDataSource(
    val apiInterface: APIInterface,
    val compositeDisposable: CompositeDisposable,
    val networkStatusLiveData: MutableLiveData<NetworkStatus>
) : PageKeyedDataSource<Int, Result>() {

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Result>) {
        networkStatusLiveData.postValue(NetworkStatus.Loading()) // show progressbar
        compositeDisposable.add(
            apiInterface.getMovies(1, Constants.Themovie_Apikey).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ response ->
                    if (!response.results.isNullOrEmpty()) {
                        networkStatusLiveData.postValue(NetworkStatus.Loaded(Constants.pageLoaded + 1)) // side progressbar, show Loaded page number in Snackbar
                        callback.onResult(response.results!!, null, 2) // update recyclerview with results
                    } else
                        networkStatusLiveData.postValue(NetworkStatus.Error(Constants.error_noData))
                },
                    {
                        networkStatusLiveData.postValue(NetworkStatus.Exception(it))
                        it.printStackTrace()
                    })
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Result>) {
        networkStatusLiveData.postValue(NetworkStatus.Loading())
        compositeDisposable.add(
            apiInterface.getMovies(params.key, Constants.Themovie_Apikey).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ response ->
                    if (!response.results.isNullOrEmpty()) {
                        networkStatusLiveData.postValue(NetworkStatus.Loaded(Constants.pageLoaded + params.key))
                        callback.onResult(response.results!!, params.key + 1)
                    } else
                        networkStatusLiveData.postValue(NetworkStatus.Error(Constants.error_noData))
                },
                    {
                        networkStatusLiveData.postValue(NetworkStatus.Exception(it))
                        it.printStackTrace()
                    })
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Result>) {
    }
}