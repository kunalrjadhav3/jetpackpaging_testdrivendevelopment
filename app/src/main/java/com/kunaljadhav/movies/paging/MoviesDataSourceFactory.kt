package com.kunaljadhav.movies.paging

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.kunaljadhav.movies.model.Result
import com.kunaljadhav.movies.retrofit.APIInterface
import com.kunaljadhav.movies.utils.NetworkStatus
import io.reactivex.disposables.CompositeDisposable

class MoviesDataSourceFactory(
    val apiInterface: APIInterface,
    val compositeDisposable: CompositeDisposable,
    val networkStatusLiveData: MutableLiveData<NetworkStatus>
) :
    DataSource.Factory<Int, Result>() {

    val moviesDataSourceLiveData = MutableLiveData<MoviesDataSource>()
    override fun create(): DataSource<Int, Result> {
        val moviesDataSource = MoviesDataSource(apiInterface, compositeDisposable,networkStatusLiveData)
        moviesDataSourceLiveData.postValue(moviesDataSource)
        return moviesDataSource
    }
}