package com.kunaljadhav.movies.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.kunaljadhav.movies.model.Result
import com.kunaljadhav.movies.retrofit.APIInterface
import com.kunaljadhav.movies.utils.Constants
import com.kunaljadhav.movies.utils.NetworkStatus
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LatestmoviesViewmodel : ViewModel() {

    var networkStatusLiveData = MutableLiveData<NetworkStatus>()
    var resultLiveData = MutableLiveData<Result>()
    val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }


    fun callGetLatestMovieApi(apiInterface: APIInterface, processScheduler: Scheduler = Schedulers.io()) {
        networkStatusLiveData.postValue(NetworkStatus.Loading()) // invoke Progressbar
        compositeDisposable.add(apiInterface.getLatestMovie(Constants.Themovie_Apikey).subscribeOn(processScheduler).subscribe { t1, t2 ->
            networkStatusLiveData.postValue(NetworkStatus.Loaded("1")) // close Progressbar
            t1?.let { handleResponse(it) }
            t2?.let { handleError(it) }
        })



    }

    private fun handleResponse(result: Result) {
        resultLiveData.postValue(result) // Pass result to View
    }

    private fun handleError(throwable: Throwable) {
        networkStatusLiveData.postValue(NetworkStatus.Exception(throwable)) // Pass exception to View
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}