package com.kunaljadhav.movies.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.kunaljadhav.movies.model.Movies
import com.kunaljadhav.movies.model.Result
import com.kunaljadhav.movies.retrofit.APIInterface
import com.kunaljadhav.movies.utils.Constants
import com.kunaljadhav.movies.utils.NetworkStatus
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MoviesByIDViewmodel : ViewModel() {
    var networkStatusLiveData = MutableLiveData<NetworkStatus>()
    val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }


    fun resgisterSearchStringSubscription(apiInterface: APIInterface, stringObservable: Observable<String>,processScheduler: Scheduler=Schedulers.io()): Observable<List<Result>> {
        return stringObservable.debounce(300, TimeUnit.MILLISECONDS).filter { it.isNotBlank() }
            .switchMap { getModifiedObservable(apiInterface,processScheduler, it) }
    }

    fun getModifiedObservable(apiInterface: APIInterface, processScheduler: Scheduler, movieId: String): Observable<List<Result>> {
        return Observable.create(object : ObservableOnSubscribe<List<Result>> {
            override fun subscribe(e: ObservableEmitter<List<Result>>) {
                networkStatusLiveData.postValue(NetworkStatus.Loading())
                compositeDisposable.add(apiInterface.getSimilarMovies(movieId, Constants.Themovie_Apikey).subscribeOn(
                    processScheduler
                ).subscribe { t1, t2 ->
                    t1?.let { handleResponse(e, it) }
                    t2?.let { handleError(it) }
                })
            }

        }).subscribeOn(processScheduler)
    }


    private fun handleResponse(e: ObservableEmitter<List<Result>>, movies: Movies) {
        movies.status_code?.let {// update UI with error message
            if (it.equals(Constants.NOResourceFound_Errorcode)) {
                movies.status_message?.let {

                    networkStatusLiveData.postValue(NetworkStatus.Error(it))
                }
            }
        }
        if (movies.results.isNullOrEmpty()) // update UI if results null
            networkStatusLiveData.postValue(NetworkStatus.Error(Constants.error_noData))

        movies.results?.let {
            // update UI with results
            networkStatusLiveData.postValue(NetworkStatus.Loaded(1.toString()))
            e.onNext(movies.results as List<Result>)
            e.onComplete()
        }



    }

    private fun handleError(error: Throwable) {
        networkStatusLiveData.postValue(NetworkStatus.Exception(error)) // update UI with exception

    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}