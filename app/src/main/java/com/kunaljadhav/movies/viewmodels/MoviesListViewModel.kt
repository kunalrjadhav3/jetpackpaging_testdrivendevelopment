package com.kunaljadhav.movies.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.kunaljadhav.movies.model.Result
import com.kunaljadhav.movies.paging.MoviesDataSourceFactory
import com.kunaljadhav.movies.repository.DBRepository
import com.kunaljadhav.movies.retrofit.APIInterface
import com.kunaljadhav.movies.room.AppDatabase
import com.kunaljadhav.movies.room.FavMoviesPojo
import com.kunaljadhav.movies.utils.DBRecordStatus
import com.kunaljadhav.movies.utils.NetworkStatus
import io.reactivex.disposables.CompositeDisposable

class MoviesListViewModel : ViewModel() {

    val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }
    val pageSize = 20
    lateinit var moviesList: LiveData<PagedList<Result>>
    private lateinit var moviesDataSourceFactory: MoviesDataSourceFactory

    var networkStatusLiveData = MutableLiveData<NetworkStatus>()

    var dBRecordStatusLiveData = MutableLiveData<DBRecordStatus>()


    fun initMoviesDataSourceFactory(apiInterface: APIInterface) {
        moviesDataSourceFactory = MoviesDataSourceFactory(apiInterface, compositeDisposable, networkStatusLiveData)
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize * 2)
            .setEnablePlaceholders(false)
            .build()
        moviesList = LivePagedListBuilder<Int, Result>(moviesDataSourceFactory, config).build()

    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun addMovieToDb(appDatabase: AppDatabase, favMovie: FavMoviesPojo) {
        DBRepository.startInsertandFetchTransaction(appDatabase, favMovie)?.let {// if insert successful, post inserted value, else null
            dBRecordStatusLiveData.postValue(DBRecordStatus.RecordInserted(it))
        }
    }

    fun deleteMovieFromDb(appDatabase: AppDatabase,favMovie: FavMoviesPojo){ // if delete successful, post True, else False
        if(DBRepository.startDeleteandCheckTransaction(appDatabase,favMovie)){
            dBRecordStatusLiveData.postValue(DBRecordStatus.RecordDeleted(favMovie))
        }
    }

}