package com.kunaljadhav.movies.di

import com.kunaljadhav.movies.views.activities.BaseActivity
import com.kunaljadhav.movies.views.fragments.BaseFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {
    fun inject(baseFragment: BaseFragment)
    fun inject(baseActivity: BaseActivity)
}