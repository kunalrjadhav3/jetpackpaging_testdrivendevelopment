package com.kunaljadhav.movies.di

import android.arch.persistence.room.Room
import android.content.Context
import com.kunaljadhav.movies.retrofit.APIInterface
import com.kunaljadhav.movies.room.AppDatabase
import com.kunaljadhav.movies.utils.Constants
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApplicationModule(val context: Context) {
    @Provides
    @Singleton
    fun getApiInterface(): APIInterface {
        val retrofit = Retrofit.Builder().baseUrl(Constants.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(APIInterface::class.java)
    }

    @Provides
    @Singleton
    fun getAppDatabase(): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, Constants.DBName)
            .build()
    }
}