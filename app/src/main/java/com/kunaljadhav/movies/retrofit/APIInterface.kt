package com.kunaljadhav.movies.retrofit

import com.kunaljadhav.movies.model.Movies
import com.kunaljadhav.movies.model.Result
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface APIInterface {
    @GET("/3/discover/movie?language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false")
    fun getMovies(@Query("page") page: Int, @Query("api_key") api_key: String): Single<Movies>

    @GET("/3/movie/{id}/similar?language=en-US&page=1")
    fun getSimilarMovies(@Path("id") id: String, @Query("api_key") api_key: String): Single<Movies>

    @GET("/3/movie/latest?language=en-US")
    fun getLatestMovie(@Query("api_key") api_key: String):Single<Result>
}

