package com.kunaljadhav.movies.firebase

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

class FireBaseInstanceIDService:FirebaseInstanceIdService() {


    val TAG by lazy { this.javaClass.simpleName }
    override fun onTokenRefresh() {
        super.onTokenRefresh()
        var refreshedToken = FirebaseInstanceId.getInstance().token
        Log.d(TAG, "Refreshed token->$refreshedToken")
    }
}