package com.kunaljadhav.movies.firebase

import android.R
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.kunaljadhav.movies.views.activities.LatestMoviesActivity


class FireBaseMessagingService : FirebaseMessagingService() {


    val TAG by lazy { this.javaClass.simpleName }
    var count: Int = 0


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        //Displaying data in log
        //It is optional
        Log.d(TAG, "Notification Message TITLE: " + remoteMessage.getNotification()!!.getTitle());
        Log.d(TAG, "Notification Message BODY: " + remoteMessage.getNotification()!!.getBody());
        Log.d(TAG, "Notification Message DATA: " + remoteMessage.getData().toString());
        sendNotification(remoteMessage.notification!!.title!!, remoteMessage.notification!!.body!!, remoteMessage.data);
    }

    //This method is only generating push notification
    private fun sendNotification(messageTitle: String, messageBody: String, row: Map<String, String>) {
        val contentIntent: PendingIntent = PendingIntent.getActivity(this,1, Intent(this,LatestMoviesActivity::class.java),PendingIntent.FLAG_CANCEL_CURRENT)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_dialog_info))
            .setSmallIcon(R.drawable.ic_dialog_info)
            .setContentTitle(messageTitle)
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(contentIntent)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(count, notificationBuilder.build())
        count++
    }
}