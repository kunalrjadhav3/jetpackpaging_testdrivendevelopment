#Name-
 Movies app
#Description-
 The project loads a list of movies from open API.
#Features-
 Lazyload list with scroll (achieved using Pagination library from Android jetpack)

 Add or remove movies from favourites section (achieved using Room DB with coroutines).

 Search similar movies from Api by typing movie ID (achieved using RX kotlin with Filter and Switchmap operator)

 Get Notifications about latest movies (achieved using Firebase push notifications)